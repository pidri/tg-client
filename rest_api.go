package main

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
)

type httpApi struct {
	tgApi     *pidriTg
	logger    *zap.Logger
	apiEngine *gin.Engine
	config    Config
}

func (api *httpApi) handleLoginCode(c *gin.Context) {
	var code = struct {
		Code string `json:"code"`
	}{}
	if err := c.BindJSON(&code); err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	api.tgApi.setAuthCode(code.Code)
	c.Status(http.StatusAccepted)

}
func (api *httpApi) handleSubscribeRequest(c *gin.Context) {
	var requestBody = struct {
		Username string `json:"username"`
	}{}
	if err := c.BindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	err := api.tgApi.joinToChannelByUsername(requestBody.Username)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
	}
	c.Status(http.StatusAccepted)
}

func (api *httpApi) run() {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", api.config.RestApiPort),
		Handler: api.apiEngine,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			api.logger.Panic("Isse with rest API", zap.Error(err))
		}
	}()
	api.logger.Info("Rest API started")
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	api.logger.Info("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		api.logger.Fatal("Server Shutdown:", zap.Error(err))
	}
	api.logger.Info("Server exiting")
}

func initHttp(logger *zap.Logger, config Config, tgApi *pidriTg) *httpApi {
	api := httpApi{
		tgApi:     tgApi,
		logger:    logger,
		apiEngine: gin.Default(),
		config:    config,
	}
	api.apiEngine.POST("/add_channel/", api.handleSubscribeRequest)
	api.apiEngine.POST("/handle_login_cide/", api.handleLoginCode)
	return &api
}
