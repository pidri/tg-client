package main

import (
	"context"
	"github.com/go-faster/errors"
	"github.com/gotd/td/tg"
	"go.uber.org/zap"

	"cloud.google.com/go/firestore"
)

var (
	CanNotSaveChannel = errors.New("CanNotSaveChannel")
)

type msgSaver struct {
	fsClient              *firestore.Client
	messegesCollection    *firestore.CollectionRef
	channelInfoCollection *firestore.CollectionRef
	logger                *zap.Logger
	context               context.Context
}

type TGMessage struct {
	Date      int    `firestore:"data"`
	Text      string `firestore:"text"`
	GroupedID int64  `firestore:"group_id"`
	ChannelID int64  `firestore:"channel_id"`
	MessageID int    `firestore:"msg_id"`
}

type TGChannelInfo struct {
	Username   string `firestore:"username"`
	JoinedUser string `firestore:"joined_user"`
	Title      string `firestore:"title"`
}

func getSaver(config Config, logger *zap.Logger) *msgSaver {
	ctx := context.Background()
	fsClient, err := firestore.NewClient(ctx, config.GrpcProjectID)
	if err != nil {
		logger.Panic("Can not init firebase client ", zap.Error(err))
	}
	return &msgSaver{
		context:               ctx,
		fsClient:              fsClient,
		logger:                logger,
		messegesCollection:    fsClient.Collection("Messages"),
		channelInfoCollection: fsClient.Collection("Channels"),
	}
}

func (s *msgSaver) saveMessage(msg *tg.Message) string {
	msgID := msg.ID
	doc, _, err := s.messegesCollection.Add(s.context, TGMessage{
		Date:      msg.Date,
		Text:      msg.Message,
		GroupedID: msg.GroupedID,
		ChannelID: msg.PeerID.(*tg.PeerChannel).ChannelID,
		MessageID: msgID,
	})
	if err != nil {
		s.logger.Error("Issue with storing doc", zap.Error(err), zap.Int("MsgID", msgID))
	}
	return doc.ID
}

func (s *msgSaver) saveChannel(channelID string, info TGChannelInfo) error {
	if _, err := s.channelInfoCollection.Doc(channelID).Set(s.context, info); err != nil {
		s.logger.Error("Cann not upsert infromation about channel", zap.Error(err))
		return CanNotSaveChannel
	}

	return nil

}

//https://t.me/asddsmckdsmcsd/28
