FROM golang:1.20.5-bookworm as build

WORKDIR /app

COPY go.* ./
RUN go mod download

COPY . ./

RUN go build -v -o tg-client

FROM debian:bookworm-slim

RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/*
    
COPY --from=build /app/tg-client /app/tg-client

WORKDIR /app

CMD ["/app/tg-client"]
