WORKDIR := $(shell pwd)
.ONESHELL:
.EXPORT_ALL_VARIABLES:
DOCKER_BUILDKIT=1


__CHECK:=$(shell \
	mkdir -p $(WORKDIR)/.build; \
)

help: ## Display help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'
	
run: build ## run client 
	docker run --rm \
		-p 8075:8075 \
		-v $(WORKDIR)/config.toml:/app/config.toml \
		-v ~/.config:/root/.config \
		tg-clinet 
# 		-v $(WORKDIR)/tg-session.json:/app/tg-session.json \
	
build: .build/img ## Build docker image 

.build/img: Dockerfile go.mod go.sum
	docker build -t tg-clinet .
	touch .build/img
	
push-stage: ## Push stage image
# 	docker tag tg-client europe-west3-docker.pkg.dev/pidri-stage/tg-client
	docker tag docker.io/library/tg-clinet  europe-west3-docker.pkg.dev/pidri-stage/pidri-stage-dkr/tg-client
	docker push europe-west3-docker.pkg.dev/pidri-stage/pidri-stage-dkr/tg-client



# e2-micro