package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	config, confError := GetConfig()
	if confError != nil {
		log.Fatalln("Can not read config", confError)
	}

	logger, _ := zap.NewDevelopment(zap.IncreaseLevel(zapcore.InfoLevel), zap.AddStacktrace(zapcore.FatalLevel))
	defer func() { _ = logger.Sync() }()

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	tgApi := getPidriTG(ctx, config, logger)

	restApi := initHttp(logger, config, tgApi)

	var wg sync.WaitGroup
	wg.Add(2)
	go func() { tgApi.runTgClientLoop(); wg.Done() }()
	go func() { restApi.run(); wg.Done() }()
	wg.Wait()

}
