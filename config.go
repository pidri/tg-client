package main

import (
	"github.com/spf13/viper"
	"os"
)

type Config struct {
	TgAppID              int    `mapstructure:"TG_APP_ID"`
	TgAppHash            string `mapstructure:"TG_APP_HASH"`
	TgLogin              string `mapstructure:"TG_LOGIN"`
	GrpcProjectID        string `mapstructure:"GRPC_PROJECT_ID"`
	RestApiPort          int    `mapstructure:"REST_API_PORT"`
	DiscordBotApi        string `mapstructure:"DSCORD_BOT_API"`
	InstanceLabel        string `mapstructure:"INSTACE_LABEL"`
	SessionStorageBucket string `mapstructure:"SESSION_STORAGE_BUCKET"`
}

func GetConfig() (config Config, err error) {
	viper.AutomaticEnv()
	if _, e := os.Stat("./config.toml"); e == nil && !os.IsNotExist(e) {
		viper.SetConfigFile("config.toml")
		viper.AddConfigPath("./")
	}

	if e := viper.ReadInConfig(); e != nil {
		return Config{}, e
	}

	if e := viper.Unmarshal(&config); e != nil {
		return Config{}, e
	}
	return config, nil
}
