package main

import (
	"context"
	"errors"
	"strings"
	"time"

	"github.com/gotd/td/telegram/auth"
	"github.com/gotd/td/tg"
)

type Authorizator struct {
	PhoneNumber string
	CodeChan    chan string
}

func (a *Authorizator) SignUp(ctx context.Context) (auth.UserInfo, error) {
	return auth.UserInfo{}, errors.New("signing up not implemented in Authorizator")
}

func (a *Authorizator) AcceptTermsOfService(ctx context.Context, tos tg.HelpTermsOfService) error {
	return &auth.SignUpRequired{TermsOfService: tos}
}

func (a *Authorizator) Code(ctx context.Context, sentCode *tg.AuthSentCode) (string, error) {
	select {
	case code := <-a.CodeChan:
		return code, nil
	case <-time.After(600 * time.Second):
		return "", errors.New("time out for code")
	}
}

func (a *Authorizator) Phone(_ context.Context) (string, error) {
	return strings.TrimSpace(a.PhoneNumber), nil
}

func (a *Authorizator) Password(_ context.Context) (string, error) {
	return "", errors.New("not implemented 2fa")
}
