package main

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"github.com/go-faster/errors"
	"go.uber.org/zap"
	"io"
	tg_session "github.com/gotd/td/session"
)

type GcpTgSessionStorage struct {
	conf Config
	logger *zap.Logger
	client *storage.Client
	bucket *storage.BucketHandle
}

func GetGcpTgSessionStorage(config Config, logger *zap.Logger) *GcpTgSessionStorage{
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		logger.Panic("Can not init storage client", zap.Error(err))
		return nil
	}
	ss:=GcpTgSessionStorage {
		conf: config,
		logger: logger,
		client: client,
		bucket: client.Bucket(config.SessionStorageBucket),
	}
	return &ss
}
func (ss *GcpTgSessionStorage) sessionFileKey() string {
	return fmt.Sprintf("%s/%s", ss.conf.TgLogin, ss.conf.InstanceLabel)
}

func (ss *GcpTgSessionStorage) LoadSession(ctx context.Context) ([]byte, error) {
	reader, err := ss.bucket.Object(ss.sessionFileKey()).NewReader(ctx);
	if err == storage.ErrObjectNotExist{
		return nil, tg_session.ErrNotFound 
	}
	defer func(reader *storage.Reader) { err = reader.Close() }(reader)
	
	if err != nil {
		err = errors.Wrap(err, "Session Storage")
		ss.logger.Error("Can not read session storage", zap.Error(err))
		return nil, err
	}
	all, err := io.ReadAll(reader)
	if err != nil {
		err = errors.Wrap(err, "Session Storage")
		return nil, err 
	}
	return all, err
	
}

func (ss *GcpTgSessionStorage) StoreSession(ctx context.Context, data []byte) error {
	writer := ss.bucket.Object(ss.sessionFileKey()).NewWriter(ctx)
	_, err := writer.Write(data)
	if err != nil {
		err = errors.Wrap(err, "Session Storage")
		ss.logger.Error("Can not stpre session storage", zap.Error(err))
		return err
	}
	return writer.Close()
}