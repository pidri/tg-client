package main

import (
	"bytes"
	"context"
	"fmt"
	"github.com/go-faster/errors"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	"github.com/gotd/td/telegram"
	"github.com/gotd/td/telegram/auth"
	"github.com/gotd/td/telegram/updates"
	updhook "github.com/gotd/td/telegram/updates/hook"
	"github.com/gotd/td/tg"
)

var (
	NickNameIsNotResolved = errors.New("NickNameIsNotResolved")
	NickNameIsNotChannel  = errors.New("NickNameIsNotChannel")
	FailToJoinToChannel   = errors.New("FailToJoinToChannel")
)

type pidriTg struct {
	logger   *zap.Logger
	ctx      context.Context
	tgClient *telegram.Client
	gaps     *updates.Manager
	saver    *msgSaver
	config   Config
	auth     *Authorizator
}

func (p *pidriTg) setAuthCode(code string)  {
	p.auth.CodeChan <- code
}

func (p *pidriTg) fetchUser() (*tg.User, error) {
	authFlow := auth.NewFlow(p.auth, auth.SendCodeOptions{})
	p.logger.Info("Auth flow runned")
	err := p.tgClient.Auth().IfNecessary(p.ctx, authFlow)
	if err != nil {
		p.logger.Error("Error during auth ", zap.Error(err))
	}

	p.logger.Info("Auth flow check")
	status, err := p.tgClient.Auth().Status(p.ctx)
	if err != nil {
		p.logger.Info("Error during auth status check", zap.Error(err))
	} else {
		p.logger.Info(fmt.Sprintf("Auth status:: %v", status.Authorized))
	}

	// Fetch user info.
	user, err := p.tgClient.Self(p.ctx)
	if err != nil {
		return nil, errors.Wrap(err, "call self")
	}
	return user, nil
}

func (p *pidriTg) channelMessageHandler(ctx context.Context, e tg.Entities, update *tg.UpdateNewChannelMessage) error {
	msg, isOk := update.Message.(*tg.Message)
	if !isOk {
		p.logger.Warn("Received message seems to be empty, ", zap.Int("msg_id", update.GetMessage().GetID()))
		return errors.New(fmt.Sprintf("Can not parse msg %d", update.GetMessage().GetID()))
	}
	p.logger.Info("Channel message", zap.Any("message", msg))
	msgID := p.saver.saveMessage(msg)
	post, err := http.Post(
		fmt.Sprintf("%s/api/handle_new_msg/", p.config.DiscordBotApi),
		"application/json",
		bytes.NewReader([]byte(fmt.Sprintf(`{ "msg_id": "%s" }`, msgID))),
	)
	if err != nil || post.StatusCode >= 300 {
		p.logger.Warn("can not trigger discord to receive msg, ", zap.Error(err))
		return err
	}
	return nil
}

func (p *pidriTg) joinToChannelByUsername(username string) error {
	usernameInfo, err := p.tgClient.API().ContactsResolveUsername(p.ctx, username)
	if err != nil {
		p.logger.Error("Can not resolve username", zap.Error(err))
		return NickNameIsNotResolved
	}
	_, ok := usernameInfo.Peer.(*tg.PeerChannel)
	if !ok {
		p.logger.Error("Can not resolve nickname to channel.", zap.Any("peer_type", fmt.Sprintf("%T", usernameInfo.Peer)))
		return NickNameIsNotChannel
	}
	chat := usernameInfo.Chats[0].(*tg.Channel)
	_, err = p.tgClient.API().ChannelsJoinChannel(p.ctx, &tg.InputChannel{ChannelID: chat.ID, AccessHash: chat.AccessHash})
	if err != nil {
		p.logger.Error("Can not join to the channel", zap.Error(err))
		return FailToJoinToChannel
	}

	err = p.saver.saveChannel(
		strconv.FormatInt(chat.ID, 10),
		TGChannelInfo{
			Username:   username,
			JoinedUser: p.config.TgLogin,
			Title:      chat.Title,
		},
	)
	if err != nil {
		return err
	}
	return nil
}

func (p *pidriTg) runTgClientLoop() {
	err := p.tgClient.Run(p.ctx, func(ctx context.Context) error {
		user, err := p.fetchUser()
		if err != nil {
			return errors.Wrap(err, "call self")
		}

		return p.gaps.Run(ctx, p.tgClient.API(), user.ID, updates.AuthOptions{
			OnStart: func(ctx context.Context) {
				p.logger.Info("Gaps started")
			},
		})
	})
	if err != nil {
		p.logger.Error("Error with runing tg client", zap.Error(err))
	}
}

func getPidriTG(ctx context.Context, config Config, logger *zap.Logger) *pidriTg {
	logger.Info("Init TG")

	updDispatcher := tg.NewUpdateDispatcher()
	gaps := updates.New(updates.Config{
		Handler: updDispatcher,
		Logger:  logger.Named("gaps"),
	})

	client := telegram.NewClient(config.TgAppID, config.TgAppHash, telegram.Options{
		Logger:         logger,
		SessionStorage: GetGcpTgSessionStorage(config, logger),
		UpdateHandler:  gaps,
		Middlewares:    []telegram.Middleware{updhook.UpdateHook(gaps.Handle)},
	})

	authorizator := Authorizator{
		PhoneNumber: config.TgLogin,
		CodeChan:    make(chan string),
	}

	p := &pidriTg{
		logger:   logger,
		ctx:      ctx,
		tgClient: client,
		gaps:     gaps,
		saver:    getSaver(config, logger),
		config:   config,
		auth:     &authorizator,
	}

	updDispatcher.OnNewChannelMessage(p.channelMessageHandler)

	return p
}
